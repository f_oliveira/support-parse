package br.com.ilhasoft.support.parse.listeners;

import com.parse.FunctionCallback;
import com.parse.ParseException;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class FunctionListener<T> extends Listener2<T> implements FunctionCallback<T> {

    @Override
    public final void done(T object, ParseException e) {
        this.doneInternal(object, e);
    }

}
