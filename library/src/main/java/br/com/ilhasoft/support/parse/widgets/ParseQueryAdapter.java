package br.com.ilhasoft.support.parse.widgets;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import br.com.ilhasoft.support.recyclerview.adapters.FilterableRecyclerAdapter;
import br.com.ilhasoft.support.recyclerview.adapters.OnDemandListener;

/**
 * Created by daniel on 30/03/16.
 */
public abstract class ParseQueryAdapter<T extends ParseObject, VH extends RecyclerView.ViewHolder>
        extends FilterableRecyclerAdapter<T, VH> implements OnDemandListener {

    private static final int DEFAULT_PAGE_SIZE = 25;

    private boolean forceLoad;
    private int currentPage = -1;
//    private ParseQuery<T> parseQuery;
    private ParseQuery<T> currentQuery;
    private boolean hasNextPage = true;
    private boolean loadingNextPage = false;
    private int pageSize = DEFAULT_PAGE_SIZE;
    private ParseQueryFactory<T> parseQueryFactory;
    private final PlusObservable<OnQueryLoadListener<T>> onQueryLoadListeners;
    private OnDemandListener onDemandListener;

    public ParseQueryAdapter() {
        this(null);
    }

    public ParseQueryAdapter(ParseQueryFactory<T> parseQueryFactory) {
        forceLoad = false;
        onQueryLoadListeners = new PlusObservable<>();
        this.parseQueryFactory = parseQueryFactory;
        super.setOnDemandListener(this);
    }

    @Override
    public long getItemId(int position) {
        if (this.size() >= position)
            return this.get(position).getObjectId().hashCode();
        return super.getItemId(position);
    }

    @Override
    public void clear() {
        super.clear();
        clearPageResults();
    }

    @Override
    public void clearObjects() {
        super.clearObjects();
        clearPageResults();
    }

    @Override
    public void setOnDemandListener(@Nullable OnDemandListener onDemandListener) {
        this.onDemandListener = onDemandListener;
    }

    @Override
    public void onLoadMore() {
        this.loadNextPage();
        if (onDemandListener != null) {
            onDemandListener.onLoadMore();
        }
    }

    public ParseQueryFactory<T> getParseQueryFactory() {
        return parseQueryFactory;
    }

    public void setParseQueryFactory(ParseQueryFactory<T> parseQueryFactory) {
        forceLoad = (parseQueryFactory != this.parseQueryFactory);
        this.parseQueryFactory = parseQueryFactory;
    }

    public void load() {
        this.clear();
        this.loadNextPage(true, false);
    }

    /**
     * It's necessary to set setHasStableIds(true) to use this functionality
     */
    public void fetch() {
        this.clearPageResults();
        this.loadNextPage(true, true);
    }

    private void clearPageResults() {
        currentPage = -1;
        hasNextPage = true;
        if (currentQuery != null) {
            currentQuery.cancel();
        }
    }

    public void load(ParseQueryFactory<T> parseQueryFactory) {
        this.setParseQueryFactory(parseQueryFactory);
        this.load();
    }

    public void loadNextPage() {
        this.loadNextPage(forceLoad, false);
    }

    public void registerOnQueryLoadListener(OnQueryLoadListener<T> listener) {
        onQueryLoadListeners.registerObserver(listener);
    }

    public void unregisterOnQueryLoadListener(OnQueryLoadListener<T> listener) {
        onQueryLoadListeners.unregisterObserver(listener);
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public boolean hasNextPage() {
        return hasNextPage;
    }

    public boolean isLoadingsNextPage() {
        return loadingNextPage;
    }

    public ParseQuery<T> getParseQuery() {
        return currentQuery;
    }

    public void setParseQuery(ParseQuery<T> parseQuery) {
//        this.parseQuery = parseQuery;
//        this.clear();
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        if (pageSize < 1) {
            this.pageSize = DEFAULT_PAGE_SIZE;
        } else {
            this.pageSize = pageSize;
        }
    }

    /**
     * Apply alterations to query prior to running findInBackground.
     */
    protected void onFilterQuery(ParseQuery<T> query) {
        // provide override for filtering query
    }

    private void loadNextPage(final boolean shouldClear, final boolean notifyAll) {
        if (parseQueryFactory == null) {
            throw new IllegalStateException("Must provide a ParseQueryFactory before loading objects.");
        }
        if (!hasNextPage) {
            return;
        }

        final int page = currentPage + 1;
        final ParseQuery<T> query = this.createQuery(page);
        query.findInBackground(new FindCallback<T>() {
            @Override
            public void done(List<T> results, ParseException e) {
                if (e == null) {
                    if (shouldClear && !notifyAll) {
                        clear();
                    }
                    onPage(query, page, results, notifyAll);
                }

                currentQuery = null;
                loadingNextPage = false;
                dispatchOnLoaded(results, e);
            }
        });

        forceLoad = false;
        currentQuery = query;
        loadingNextPage = true;
        this.dispatchOnLoading();
    }

    private void onPage(ParseQuery<T> query, int page, List<T> results, boolean notifyAll) {
        if (results == null || results.isEmpty()) {
            if (notifyAll) setList(new ArrayList<>());
            return;
        }

        int itemCount = results.size();

        currentPage = page;
        int limit = query.getLimit();
        if (limit == -1 || limit == pageSize) {
            // Backwards compatibility hack to support ParseQueryAdapter#setPaginationEnabled(false)
            hasNextPage = false;
        } else {
            // We detect if there are more pages by setting the limit pageSize + 1 and we remove the extra
            // if there are more pages.
            hasNextPage = itemCount >= pageSize + 1;
            if (itemCount > pageSize) {
                results.remove(pageSize);
            }
        }
        int objectsSize = this.size();
        boolean inserted = true;
        if (objectsSize > pageSize * page) {
            inserted = false;
            this.subList(pageSize * page, Math.min(objectsSize, pageSize * (page + 1))).clear();
        }

        if (notifyAll) {
            this.setList(results);
        } else {
            this.addAll(pageSize * page, results);
            int positionStart = pageSize * page;
            if (inserted) {
                this.notifyItemRangeInserted(positionStart, itemCount);
            } else {
                this.notifyItemRangeChanged(positionStart, itemCount);
            }
        }
    }

    private ParseQuery<T> createQuery(int page) {
        // TODO: Update the parse sdk to use a new constructor instead of this
        ParseQuery<T> query = parseQueryFactory.create();
//        ParseQuery<T> query = new ParseQuery<>(parseQuery);
        query.setSkip(pageSize * page);
        // Limit is pageSize + 1 so we can detect if there are more pages
        query.setLimit(pageSize + 1);

        this.onFilterQuery(query);

        return query;
    }

    private void dispatchOnLoading() {
        for (OnQueryLoadListener<T> listener : onQueryLoadListeners.getObservers()) {
            listener.onLoading();
        }
    }

    private void dispatchOnLoaded(List<T> objects, ParseException e) {
        for (OnQueryLoadListener<T> listener : onQueryLoadListeners.getObservers()) {
            listener.onLoaded(objects, e);
        }
    }

    /**
    * Implement with logic that is called before and after objects are fetched from Parse by the
    * adapter.
    */
    public interface OnQueryLoadListener<T extends ParseObject> {
        void onLoading();
        void onLoaded(List<T> parseObjects, ParseException e);
    }

}
