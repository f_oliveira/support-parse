package br.com.ilhasoft.support.parse.listeners;

import com.parse.GetFileCallback;
import com.parse.ParseException;

import java.io.File;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class GetFileListener extends Listener2<File> implements GetFileCallback {

    @Override
    public final void done(File file, ParseException e) {
        this.doneInternal(file, e);
    }

}
