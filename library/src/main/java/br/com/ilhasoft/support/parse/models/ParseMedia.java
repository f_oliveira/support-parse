package br.com.ilhasoft.support.parse.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;

import com.parse.ParseClassName;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;
import com.parse.ParseFile;

/**
 * Created by john-mac on 10/7/16.
 */
@ParseClassName("Media")
public class ParseMedia extends ParseObject implements Parcelable {

    public static final String KEY_TYPE = "type";
    public static final String KEY_MEDIA_FILE = "mediaFile";
    public static final String KEY_THUMBNAIL = "thumbnail";
    public static final String KEY_YOUTUBE_CODE = "youtubeCode";
    public static final String KEY_METADATA = "metadata";

    public static final String TYPE_VIDEO = "video";
    public static final String TYPE_FILE = "file";
    public static final String TYPE_AUDIO = "audio";
    public static final String TYPE_PICTURE = "picture";
    public static final String TYPE_YOUTUBE = "youtube";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({TYPE_VIDEO, TYPE_PICTURE, TYPE_YOUTUBE, TYPE_FILE, TYPE_AUDIO})
    public @interface MediaType {}

    @MediaType
    @SuppressWarnings("WrongConstant")
    public String getType() {
        return getString(KEY_TYPE);
    }

    public void setType(@MediaType String type) {
        this.put(KEY_TYPE, type);
    }

    public ParseFile getMediaFile() {
        return getParseFile(KEY_MEDIA_FILE);
    }

    public void setMediaFile(ParseFile mediaFile) {
        this.put(KEY_MEDIA_FILE, mediaFile);
    }

    public ParseFile getThumbnail() {
        return getParseFile(KEY_THUMBNAIL);
    }

    public void setThumbnail(ParseFile thumbnail) {
        this.put(KEY_THUMBNAIL, thumbnail);
    }

    public String getYoutubeCode() {
        return getString(KEY_YOUTUBE_CODE);
    }

    public void setYoutubeCode(String youtubeCode) {
        this.put(KEY_YOUTUBE_CODE, youtubeCode);
    }

    public void setMetadata(Map<String, String> metadata) {
        put(KEY_METADATA, metadata);
    }

    public Map<String, String> getMetadata() {
        return getMap(KEY_METADATA);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getObjectId());
        dest.writeString(getType());
        dest.writeParcelable(getThumbnail(), flags);
        dest.writeParcelable(getMediaFile(), flags);
        dest.writeString(getYoutubeCode());

        int metadataSize = getMetadata() != null ? getMetadata().size() : 0;
        dest.writeInt(metadataSize);
        if (metadataSize > 0) {
            for (Map.Entry<String, String> entry : getMetadata().entrySet()) {
                dest.writeString(entry.getKey());
                dest.writeString(entry.getValue());
            }
        }
    }

    public ParseMedia() {
    }

    @SuppressWarnings("WrongConstant")
    protected ParseMedia(Parcel in) {
        setObjectId(in.readString());
        setType(in.readString());
        setThumbnail(in.readParcelable(ParseFile.class.getClassLoader()));
        setMediaFile(in.readParcelable(ParseFile.class.getClassLoader()));
        setYoutubeCode(in.readString());
        int metadataSize = in.readInt();
        Map<String, String> metadata = new HashMap<>(metadataSize);
        for (int i = 0; i < metadataSize; i++) {
            String key = in.readString();
            String value = in.readString();
            metadata.put(key, value);
        }
        setMetadata(metadata);
    }

    public static final Creator<ParseMedia> CREATOR = new Creator<ParseMedia>() {
        @Override
        public ParseMedia createFromParcel(Parcel source) {
            return new ParseMedia(source);
        }

        @Override
        public ParseMedia[] newArray(int size) {
            return new ParseMedia[size];
        }
    };
}
