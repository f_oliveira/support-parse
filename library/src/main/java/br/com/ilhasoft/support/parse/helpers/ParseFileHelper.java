package br.com.ilhasoft.support.parse.helpers;

import android.graphics.Bitmap;

import com.parse.ParseFile;

import java.io.ByteArrayOutputStream;

import rx.Observable;

/**
 * Created by john-mac on 9/24/16.
 */
public class ParseFileHelper {

    public static Observable<ParseFile> getParseFileFromBitmap(Bitmap imageBitmap) {
        return Observable.create(subscriber -> {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] imageByte = byteArrayOutputStream.toByteArray();
                subscriber.onNext(new ParseFile("image_file.png", imageByte));
                subscriber.onCompleted();
            } catch (Exception exception) {
                subscriber.onError(exception);
            }
        });
    }

}
