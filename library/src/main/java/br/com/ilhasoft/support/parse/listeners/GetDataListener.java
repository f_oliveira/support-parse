package br.com.ilhasoft.support.parse.listeners;

import com.parse.GetDataCallback;
import com.parse.ParseException;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class GetDataListener extends Listener2<byte[]> implements GetDataCallback {

    @Override
    public final void done(byte[] data, ParseException e) {
        this.doneInternal(data, e);
    }

}
