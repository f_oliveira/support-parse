package br.com.ilhasoft.support.parse.listeners;

import com.parse.GetDataStreamCallback;
import com.parse.ParseException;

import java.io.InputStream;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class GetDataStreamListener extends Listener2<InputStream> implements GetDataStreamCallback {

    @Override
    public final void done(InputStream inputStream, ParseException e) {
        this.doneInternal(inputStream, e);
    }

}
