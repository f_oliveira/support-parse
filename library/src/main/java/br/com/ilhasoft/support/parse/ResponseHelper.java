package br.com.ilhasoft.support.parse;

import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.util.Log;

import com.parse.ParseException;

/**
 * Created by danielsan on 8/25/15.
 */
public final class ResponseHelper {

    public static final int NO_ERROR = -1;

    private static final String TAG = "ResponseHelper";

    private ResponseHelper() {
        throw new UnsupportedOperationException("This is a pure static class!");
    }

    /**
     * @param e that will be handled.
     * @return error string resource id if parseException is contains a error,
     *         otherwise {@link ResponseHelper#NO_ERROR}.
     */
    @StringRes
    public static int getErrorMessageId(@Nullable ParseException e) {
        if (e == null) {
            return NO_ERROR;
        }

        Log.w(TAG, "getErrorMessageId: ", e);
        switch (e.getCode()) {
            case ParseException.INVALID_EMAIL_ADDRESS:
                return R.string.error_message_invalid_email_address;
            case ParseException.EMAIL_TAKEN:
            case ParseException.USERNAME_TAKEN:
                return R.string.error_message_email_taken;
            case ParseException.EMAIL_NOT_FOUND:
                return R.string.error_message_email_not_found;
            case ParseException.CONNECTION_FAILED:
                return R.string.error_message_connection_failed;
            case ParseException.OBJECT_NOT_FOUND:
                return R.string.error_message_object_not_found;
            case ParseException.CACHE_MISS:
                return NO_ERROR;
            default:
                return R.string.error_message_unexpected_error;
        }
    }

    /**
     * @param e the exception that be checked.
     * @return false if parseException is null or ParseException#CACHE_MISS, otherwise true.
     */
    public static boolean hasAnyError(@Nullable ParseException e) {
        return (ResponseHelper.getErrorMessageId(e) != ResponseHelper.NO_ERROR);
    }

}
