package br.com.ilhasoft.support.parse.models;

import java.io.File;

/**
 * Created by john-mac on 10/7/16.
 */
public class ParseFile extends com.parse.ParseFile {

    private File localFile;

    public ParseFile(File localFile) {
        super(localFile);
        this.localFile = localFile;
    }

    public File getLocalFile() {
        return localFile;
    }
}
