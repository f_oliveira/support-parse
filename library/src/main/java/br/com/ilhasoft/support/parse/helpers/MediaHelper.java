package br.com.ilhasoft.support.parse.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.parse.ParseFile;

import java.io.File;

import br.com.ilhasoft.support.core.helpers.IoHelper;
import br.com.ilhasoft.support.graphics.BitmapCompressor;
import br.com.ilhasoft.support.graphics.BitmapHelper;
import br.com.ilhasoft.support.parse.models.ParseMedia;
import br.com.ilhasoft.support.rxgraphics.FileCompressor;
import rx.Observable;
import rx.parse.ParseObservable;

/**
 * Created by john-mac on 11/23/16.
 */

public class MediaHelper {

    private static final String TAG = "MediaHelper";

    private static final int THUMBSIZE = 400;

    /**
     * Method used to save media with thumbnail if needed
     * @param context
     * @param mediaObservable
     * @return
     */
    public static Observable<ParseMedia> saveMedia(Context context, Observable<ParseMedia> mediaObservable) {
        return mediaObservable.concatMap(media -> MediaHelper.createThumbnail(context, media))
                .concatMap(MediaHelper::getMediaCompressObservable)
                .doOnNext(media -> ParseObservable.save(media.getThumbnail()))
                .doOnNext(media -> ParseObservable.save(media.getMediaFile()))
                .flatMap(ParseObservable::save);
    }

    @NonNull
    public static Observable<ParseMedia> createThumbnail(Context context, final ParseMedia parseMedia) {
        try {
            File file = ((br.com.ilhasoft.support.parse.models.ParseFile)parseMedia.getMediaFile()).getLocalFile();
            if (parseMedia.getType().equals(ParseMedia.TYPE_VIDEO)) {
                Bitmap bitmap = BitmapHelper.getThumbnailFromVideoUri(context, Uri.fromFile(file));
                File createdFile = IoHelper.createImageFilePath(context);

                File thumbnailFile = BitmapCompressor.setBitmapToNewFileCompressed(bitmap, createdFile);
                parseMedia.setThumbnail(new ParseFile(thumbnailFile));
            } else if (parseMedia.getType().equals(ParseMedia.TYPE_PICTURE)) {
                Bitmap bitmap = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(file.getAbsolutePath())
                        , THUMBSIZE, THUMBSIZE);

                File thumbnailFile = IoHelper.createImageFilePath(context);
                BitmapCompressor.setBitmapToNewFileCompressed(bitmap, thumbnailFile);

                parseMedia.setThumbnail(new ParseFile(thumbnailFile));
            }
        } catch(Exception exception) {
            Log.e(TAG, "call: ", exception);
        }
        return Observable.just(parseMedia);
    }

    private static Observable<ParseMedia> getMediaCompressObservable(ParseMedia parseMedia) {
        Observable<File> compressObservable;
        br.com.ilhasoft.support.parse.models.ParseFile parseFile =
                (br.com.ilhasoft.support.parse.models.ParseFile) parseMedia.getMediaFile();
        if (parseMedia.getType().equals(ParseMedia.TYPE_VIDEO)) {
            compressObservable = FileCompressor.getVideoCompressedObservable(parseFile.getLocalFile());
            return completeMediaFileCompressed(parseMedia, compressObservable);
        } else if (parseMedia.getType().equals(ParseMedia.TYPE_PICTURE)) {
            compressObservable = FileCompressor.getPictureCompressedObservable(parseFile.getLocalFile());
            return completeMediaFileCompressed(parseMedia, compressObservable);
        } else {
            return Observable.just(parseMedia);
        }
    }

    private static Observable<ParseMedia> completeMediaFileCompressed(ParseMedia parseMedia, Observable<File> compressObservable) {
        return compressObservable.flatMap(file -> {
            parseMedia.setMediaFile(new ParseFile(file));
            return Observable.just(parseMedia);
        });
    }

}
