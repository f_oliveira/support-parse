package br.com.ilhasoft.support.parse.auth.facebook;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;

import org.json.JSONObject;

import rx.Observable;

/**
 * Created by john-mac on 11/23/16.
 */
public class RequestFacebookData {

    public static final String DEFAULT_FIELDS = "id, name, email, gender, birthday, picture.type(large)";

    public static Observable<JSONObject> getFacebookUserInformation() {
        return getFacebookUserInformation(DEFAULT_FIELDS);
    }

    /**
     * Request user information from Facebook
     * @param fields Facebook user fields separated by comma (Ex. id, name, email, gender)
     * @return JSONObject with data collected
     */
    public static Observable<JSONObject> getFacebookUserInformation(final String fields) {
        return Observable.create(subscriber -> {
            try {
                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), null);

                Bundle parameters = new Bundle();
                parameters.putString("fields", fields);
                request.setParameters(parameters);
                JSONObject object = request.executeAndWait().getJSONObject();

                subscriber.onNext(object);
                subscriber.onCompleted();
            } catch (Exception exception) {
                subscriber.onError(exception);
            }
        });
    }

}
