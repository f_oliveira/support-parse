package br.com.ilhasoft.support.parse.listeners;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class LogInListener extends Listener2<ParseUser> implements LogInCallback {

    @Override
    public final void done(ParseUser user, ParseException e) {
        this.doneInternal(user, e);
    }

}
