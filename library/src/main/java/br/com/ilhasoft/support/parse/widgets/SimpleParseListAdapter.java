package br.com.ilhasoft.support.parse.widgets;

import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.*;

import java.util.ArrayList;
import java.util.List;

import br.com.ilhasoft.support.parse.ResponseHelper;

/**
 * Created by daniel on 03/06/16.
 */
public class SimpleParseListAdapter<T extends ParseObject> extends BaseAdapter {

    private String column;
    @LayoutRes int layoutId;
    private ParseQuery<T> query;
    private final List<T> objects;
    private FindCallback<T> findCallback;

    public SimpleParseListAdapter(Class<T> clazz, String column) {
        this(ParseQuery.getQuery(clazz), column, android.R.layout.simple_spinner_dropdown_item);
    }

    public SimpleParseListAdapter(ParseQuery<T> parseQuery, String column) {
        this(parseQuery, column, android.R.layout.simple_spinner_dropdown_item);
    }

    public SimpleParseListAdapter(ParseQuery<T> parseQuery, String column,
                                  @LayoutRes int layoutIdRes) {
        this.column = column;
        objects = new ArrayList<>();
        layoutId = layoutIdRes;
        query = parseQuery;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public T getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return objects.size() > 0 ? objects.get(position).getObjectId().hashCode() : 0;
    }

    @Override
    @SuppressWarnings("unchecked")
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
            viewHolder = new ViewHolder(convertView);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.bind(objects.get(position));
        return convertView;
    }

    public void add(int position, T item) {
        objects.add(position, item);
        notifyDataSetChanged();
    }

    public void add(T item) {
        objects.add(item);
        notifyDataSetChanged();
    }

    public void remove(T item) {
        objects.remove(item);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        objects.remove(position);
        notifyDataSetChanged();
    }

    public void load() {
        query.findInBackground(findCallbackInternal);
    }

    public void setQuery(ParseQuery<T> parseQuery) {
        query = parseQuery;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public void setLayoutId(@LayoutRes int layoutId) {
        this.layoutId = layoutId;
    }

    public FindCallback<T> getFindCallback() {
        return findCallback;
    }

    public void setFindCallback(FindCallback<T> findCallback) {
        this.findCallback = findCallback;
    }

    private final FindCallback<T> findCallbackInternal = new FindCallback<T>() {
        @Override
        public void done(List<T> list, ParseException e) {
            objects.clear();
            if (ResponseHelper.getErrorMessageId(e) == ResponseHelper.NO_ERROR
                    && list != null && !list.isEmpty()) {
                objects.addAll(list);
            }
            notifyDataSetChanged();
            if (findCallback != null) {
                findCallback.done(list, e);
            }
        }
    };

    private class ViewHolder {
        private final TextView textView;
        public ViewHolder(View view) {
            view.setTag(this);
            if (view instanceof TextView) {
                textView = (TextView) view;
            } else {
                textView = (TextView) view.findViewById(android.R.id.text1);
                if (textView == null) {
                    throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView");
                }
            }
        }
        public void bind(T object) {
            textView.setText(object.getString(column));
        }
    }

}
