package br.com.ilhasoft.support.parse.listeners;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.RefreshCallback;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class RefreshListener extends Listener2<ParseObject> implements RefreshCallback {

    @Override
    public final void done(ParseObject object, ParseException e) {
        this.doneInternal(object, e);
    }

}
