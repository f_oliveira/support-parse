package br.com.ilhasoft.support.parse.listeners;

import com.parse.ConfigCallback;
import com.parse.ParseConfig;
import com.parse.ParseException;

/**
 * Created by daniel on 23/05/16.
 */
public abstract class ConfigListener extends Listener2<ParseConfig> implements ConfigCallback {

    @Override
    public final void done(ParseConfig config, ParseException e) {
        this.doneInternal(config, e);
    }

}
