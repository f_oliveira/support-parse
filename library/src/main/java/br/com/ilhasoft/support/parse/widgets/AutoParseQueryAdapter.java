package br.com.ilhasoft.support.parse.widgets;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.parse.ParseObject;

import br.com.ilhasoft.support.recyclerview.adapters.AutoRecyclerAdapterDelegate;
import br.com.ilhasoft.support.recyclerview.adapters.OnBindViewHolder;
import br.com.ilhasoft.support.recyclerview.adapters.OnCreateViewHolder;
import br.com.ilhasoft.support.recyclerview.adapters.ViewHolder;

/**
 * Created by daniel on 16/06/16.
 */
public class AutoParseQueryAdapter<T extends ParseObject, VH extends ViewHolder<T>>
        extends ParseQueryAdapter<T, VH> {

    private final AutoRecyclerAdapterDelegate<T, VH> delegate;

    public AutoParseQueryAdapter(OnCreateViewHolder<T, VH> onCreateViewHolder) {
        super();
        delegate = new AutoRecyclerAdapterDelegate<>(onCreateViewHolder);
    }

    public AutoParseQueryAdapter(ParseQueryFactory<T> parseQueryFactory,
                                 OnCreateViewHolder<T, VH> onCreateViewHolder) {
        super(parseQueryFactory);
        delegate = new AutoRecyclerAdapterDelegate<>(onCreateViewHolder);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        delegate.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return delegate.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        delegate.onBindViewHolder(holder, this.get(position), position);
    }

    @Nullable
    public OnBindViewHolder<T, VH> getOnBindViewHolder() {
        return delegate.getOnBindViewHolder();
    }

    public void setOnBindViewHolder(@Nullable OnBindViewHolder<T, VH> onBindViewHolder) {
        delegate.setOnBindViewHolder(onBindViewHolder);
    }

}
