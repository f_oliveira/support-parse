# IlhaSoft Support Parse CHANGELOG

## Version 0.2.0 (2016-06-17)

* Added SimpleParseListAdapter for simple use cases;
* Implemented AutoParseQueryAdapter;
* Updated support core library;
* Implemented basic ParseObjects that implement toString, equals and hashCode;
* Added changelog;

## Version 0.1.4 (2016-06-08)

* Add hybrid save listener for both signup and save callbacks;

## Version 0.1.3 (2016-05-30)

* Updated support core library;

## Version 0.1.2 (2016-05-29)

* Fixed build on some projects (duplicated dependences of bolts libraries);
* Fixed `DeleteListener` and renamed `RequestResponseHelper` to `ResponseHelper`;
* Added **hasAnyError** method to `ResponseHelper`;

## Version 0.1.1 (2016-05-26)

* Fixed duplicated entry and update support core library;
* Adjust listener method names;

## Version 0.1.0 (2016-05-23)

* **Initial release**;
* Created awesome classes:
    * `ParseQueryAdapter` to easy implement recycler adapters with less code, and with useful features;
    * `LocalRequestHelper` allow to query locally and remotely;
    * `RequestResponseHelper` provide so easy friendly error messages when there is;
    * `SelectParseObjectDialog` is a simple way to select a **ParseObject** from a query;
* Created useful listeners for easy handler parse callbacks;