package br.com.ilhasoft.support.parse.helpers;

import android.support.annotation.NonNull;

import com.parse.ParseInstallation;
import com.parse.ParseUser;

import rx.Observable;
import rx.parse.ParseObservable;

/**
 * Created by john-mac on 11/23/16.
 */

public final class ParseInstallationHelper {

    @NonNull
    public static Observable<ParseInstallation> updateInstallationWithUser(ParseUser user) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("user", user);

        return ParseObservable.save(installation);
    }

}
