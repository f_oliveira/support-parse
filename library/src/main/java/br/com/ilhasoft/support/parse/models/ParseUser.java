package br.com.ilhasoft.support.parse.models;

/**
 * Created by daniel on 16/06/16.
 */
public class ParseUser extends com.parse.ParseUser {

    @Override
    public String toString() {
        return getClass().getName() + "@" + getClassName() + "{objectId='" +
                getObjectId() + "'}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof com.parse.ParseUser)) return false;

        com.parse.ParseUser parseUser = (com.parse.ParseUser) o;

        return getObjectId() != null ? getObjectId().equals(parseUser.getObjectId()) : parseUser.getObjectId() == null;

    }

    @Override
    public int hashCode() {
        return getObjectId() != null ? getObjectId().hashCode() : 0;
    }

}
