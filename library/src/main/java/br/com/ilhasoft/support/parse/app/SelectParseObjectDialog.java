package br.com.ilhasoft.support.parse.app;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseObject;
import com.parse.ParseQuery;

import br.com.ilhasoft.support.core.text.EmptyTextWatcher;
import br.com.ilhasoft.support.parse.R;
import br.com.ilhasoft.support.parse.databinding.DialogSelectParseObjectBinding;
import br.com.ilhasoft.support.parse.widgets.ParseQueryAdapter;
import br.com.ilhasoft.support.parse.widgets.ParseQueryFactory;
import br.com.ilhasoft.support.recyclerview.adapters.ViewHolder;

/**
 * Created by daniel on 18/04/16.
 */
public class SelectParseObjectDialog<Obj extends ParseObject> extends AlertDialog {

    private static final int DELAY_TO_SEARCH = 300;

    private String text;
    private final String column;
    private Handler handlerSearch;
    private final boolean showClear;
    private final CharSequence title;
    private final Listener<Obj> listener;
    private final ParseQueryFactory<Obj> parseQueryFactory;
    private final ParseObjectQueryAdapter<Obj> parseObjectQueryAdapter;
    private DialogSelectParseObjectBinding binding;

    public static <Obj extends ParseObject> SelectParseObjectDialog<Obj> show(Context context, CharSequence title,
                                                                        ParseQueryFactory<Obj> parseQueryFactory,
                                                                        String column, Listener<Obj> listener) {
        return SelectParseObjectDialog.show(context, title, parseQueryFactory, column, true, listener);
    }

    public static <Obj extends ParseObject> SelectParseObjectDialog<Obj> show(Context context, CharSequence title,
                                                                        ParseQueryFactory<Obj> parseQueryFactory,
                                                                        String column, boolean showClear, Listener<Obj> listener) {
        SelectParseObjectDialog<Obj> selectParseObjectDialog = new SelectParseObjectDialog<Obj>(context, title, parseQueryFactory, column, showClear, listener);
        selectParseObjectDialog.show();
        return selectParseObjectDialog;
    }

    public SelectParseObjectDialog(Context context, CharSequence title, ParseQueryFactory<Obj> parseQueryFactory,
                                    String column, boolean showClear, Listener<Obj> listener) {
        super(context);
        this.title = title;
        this.column = column;
        this.listener = listener;
        this.showClear = showClear;
        this.parseQueryFactory = parseQueryFactory;
        parseObjectQueryAdapter = new ParseObjectQueryAdapter<>(column, clickListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DialogSelectParseObjectBinding.inflate(LayoutInflater.from(this.getContext()), null, false);

        binding.etText.addTextChangedListener(simpleTextWatcher);
        binding.rvObjects.setLayoutManager(new LinearLayoutManager(this.getContext()));
        binding.rvObjects.setAdapter(parseObjectQueryAdapter);
        binding.rvObjects.setHasFixedSize(true);

        parseObjectQueryAdapter.load(parseQueryFactoryInternal);

        this.setTitle(title);
        this.setView(binding.getRoot());
        this.setButton(BUTTON_POSITIVE, this.getContext().getText(android.R.string.ok), clickDialogButtons);
        this.setButton(BUTTON_NEGATIVE, this.getContext().getText(android.R.string.cancel), clickDialogButtons);
        if (showClear) {
            this.setButton(BUTTON_NEUTRAL, this.getContext().getText(R.string.clear), clickDialogButtons);
        }

        super.onCreate(savedInstanceState);
    }

    private final ParseQueryFactory<Obj> parseQueryFactoryInternal = new ParseQueryFactory<Obj>() {
        @Override
        public ParseQuery<Obj> create() {
            if (TextUtils.isEmpty(text)) {
                return parseQueryFactory.create();
            } else {
                return parseQueryFactory.create().whereMatches(column, text, "i");
            }
        }
    };

    private final EmptyTextWatcher simpleTextWatcher = new EmptyTextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            super.onTextChanged(s, start, before, count);
            text = TextUtils.isEmpty(s) ? null : s.toString();
            if (handlerSearch == null) {
                handlerSearch = new Handler();
                handlerSearch.postDelayed(runnableSearch, DELAY_TO_SEARCH);
            }
        }
    };

    private final ClickListener<Obj> clickListener = new ClickListener<Obj>() {
        @Override
        public void onClick(Obj obj) {
            if (listener != null) {
                listener.onObjectSelected(obj);
            }
            dismiss();
        }
    };

    private final OnClickListener clickDialogButtons = new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (listener == null) {
                return;
            }
            switch (which) {
                case BUTTON_POSITIVE:
                    listener.onTextSelected(binding.etText.getText().toString().trim());
                    break;
                case BUTTON_NEGATIVE:
                    listener.onCanceled();
                    break;
                case BUTTON_NEUTRAL:
                    listener.onTextSelected(null);
                    break;
            }
        }
    };

    private final Runnable runnableSearch = new Runnable() {
        @Override
        public void run() {
            handlerSearch = null;
            parseObjectQueryAdapter.load();
        }
    };

    public interface ClickListener<Obj extends ParseObject> {
        void onClick(Obj obj);
    }

    public interface Listener<Obj extends ParseObject> {
        void onCanceled();
        void onTextSelected(String text);
        void onObjectSelected(Obj obj);
    }

    private static final class ParseObjectQueryAdapter<Obj extends ParseObject> extends ParseQueryAdapter<Obj, ParseObjectViewHolder<Obj>> {
        private final String column;
        private LayoutInflater layoutInflater;
        private final ClickListener<Obj> clickListener;
        private ParseObjectQueryAdapter(String column, ClickListener<Obj> clickListener) {
            this.column = column;
            this.clickListener = clickListener;
        }
        @Override
        public void onAttachedToRecyclerView(RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
            layoutInflater = LayoutInflater.from(recyclerView.getContext());
        }
        @Override
        public ParseObjectViewHolder<Obj> onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ParseObjectViewHolder<>(layoutInflater.inflate(R.layout.item_text_dialog, parent, false), column, clickListener);
        }
        @Override
        public void onBindViewHolder(ParseObjectViewHolder<Obj> holder, int position) {
            holder.bind(this.get(position));
        }
    }

    private static final class ParseObjectViewHolder<Obj extends ParseObject> extends ViewHolder<Obj>
            implements View.OnClickListener {
        private final String column;
        private final ClickListener<Obj> clickListener;
        public ParseObjectViewHolder(View itemView, String column, ClickListener<Obj> clickListener) {
            super(itemView);
            this.column = column;
            this.clickListener = clickListener;
            itemView.setOnClickListener(this);
        }
        @Override
        protected void onBind(Obj object) {
            if (TextUtils.isEmpty(column)) {
                ((TextView) itemView).setText(object.toString());
            } else {
                ((TextView) itemView).setText(object.getString(column));
            }
        }
        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                clickListener.onClick(this.getObject());
            }
        }
    }

}
